using System.IO;
using Cysharp.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Integrations.Firebase.Tools.DTOs;
using Newtonsoft.Json;
using UnityEngine;

namespace Integrations.Firebase.Tools
{
    internal static class AuthService
    {
        public static string AuthToken { get; private set; }

        public static bool HasAuthToken => !string.IsNullOrEmpty(AuthToken);
        
        public static async UniTask<string> RefreshAuthToken()
        {
            var credentialsPath = $"{Application.dataPath.Replace("Assets", "UserSettings")}/service-account-credentials.json";
            var credentialsJson = await File.ReadAllTextAsync(credentialsPath);
            var credential = JsonConvert.DeserializeObject<Credential>(credentialsJson);
            var scopes = new[] {"https://www.googleapis.com/auth/firebase.remoteconfig"};

            var initializer = new ServiceAccountCredential.Initializer(credential.ClientEmail) 
                {Scopes = scopes}.FromPrivateKey(credential.PrivateKey);
        
            var serviceCredential = new ServiceAccountCredential(initializer);
            AuthToken = await serviceCredential.GetAccessTokenForRequestAsync();
            return AuthToken;
        }
    }
}
