﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using Configuration.Tools;
using Cysharp.Threading.Tasks;
using Infrastructure.Configuration;
using Integrations.Firebase.Tools.DTOs;
using Newtonsoft.Json;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace Integrations.Firebase.Tools
{
    public class FirebaseRemoteConfigBackend : IRemoteConfigBackend
    {
        private static string _lastETag;
        private Template _template;

        [InitializeOnLoadMethod]
        private static void Register()
        {
            RemoteConfigBackend.RegisterInstance(new FirebaseRemoteConfigBackend());
        }

        private async UniTask Push()
        {
            if (!AuthService.HasAuthToken)
            {
                await AuthService.RefreshAuthToken();
            }

            var body = JsonConvert.SerializeObject(_template, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            var response = await PutTemplateJson(body);
            var json = GetJsonOrThrow(response);
            CacheResult(json);
            _template = JsonConvert.DeserializeObject<Template>(json);
            _lastETag = response.GetResponseHeader("ETag");
        }

        private static UnityWebRequestAsyncOperation PutTemplateJson(string body)
        {
            var projectName = "projects/testing-ground-d4494";
            var uri = $"https://firebaseremoteconfig.googleapis.com/v1/{projectName}/remoteConfig";
            var request = UnityWebRequest.Put(uri, body);
            request.SetRequestHeader("Authorization", "Bearer " + AuthService.AuthToken);
            request.SetRequestHeader("If-Match", "*"); //Since we are unable to get ETag, we use wild card (*)
            return request.SendWebRequest();
        }

        public async UniTask<IEnumerable<RemoteProperty>> FetchAll()
        {
            if (!AuthService.HasAuthToken)
            {
                await AuthService.RefreshAuthToken();
            }

            var response = await FetchTemplateJson();
            var json = GetJsonOrThrow(response);
            CacheResult(json);
            _template = JsonConvert.DeserializeObject<Template>(json);
            _lastETag = response.GetResponseHeader("ETag");
            return GetDefaultProperties(_template);
        }

        public async UniTask Update(IEnumerable<RemoteProperty> properties)
        {
            foreach (var property in properties)
            {
                Commit(property);
            }

            await Push();
        }
        
        private static UnityWebRequestAsyncOperation FetchTemplateJson()
        {
            var projectName = "projects/testing-ground-d4494";
            var uri = $"https://firebaseremoteconfig.googleapis.com/v1/{projectName}/remoteConfig";
            var request = UnityWebRequest.Get(uri);
            request.SetRequestHeader("Authorization", "Bearer " + AuthService.AuthToken);
            return request.SendWebRequest();
        }
        
        private static string GetJsonOrThrow(UnityWebRequest response)
        {
            switch (response.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                    Debug.LogError("Error: " + response.error);
                    throw new HttpRequestException();
                
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError("Error: " + response.error);
                    throw new HttpRequestException();

                case UnityWebRequest.Result.Success:
                    return response.downloadHandler.text;
            }
            
            throw new HttpRequestException();
        }
        
        private void Commit(RemoteProperty property)
        {
            if (_template == null)
                return;

            _template.Organise();
            var featureName = property.FeatureName;
            var prefix = $"{featureName}_";

            Dictionary<string, Parameter> parameterGroup;
            if (string.IsNullOrEmpty(featureName))
            {
                parameterGroup = _template.Parameters;
                prefix = "";
            }
            else
            {
                if(!_template.ParameterGroups.ContainsKey(featureName))
                    _template.ParameterGroups.Add(featureName, new ParameterGroup());

                parameterGroup = _template.ParameterGroups[featureName].Parameters;
            }

            var name = $"{prefix}{property.PropertyName}";
            if (parameterGroup.ContainsKey(name))
            {
                var existingParameter = parameterGroup[name];
                existingParameter.DefaultValue.Value = property.JsonValue;
            }
            else
            {
                var newParameter = new Parameter
                {
                    DefaultValue = new ParameterValue { Value = property.JsonValue },
                    ValueType = "STRING" //TODO: We may want to infer this somehow
                };
                    
                parameterGroup.Add(name, newParameter);
            }
        }
        
        private static IEnumerable<RemoteProperty> GetDefaultProperties(Template template)
        {
            var parameters = template.GetAllParameters();
            foreach (var (name, parameter) in parameters)
            {
                if (parameter.DefaultValue == null)
                    continue;

                yield return new RemoteProperty
                {
                    FeatureName = Utility.GetFeatureName(name),
                    PropertyName = Utility.GetPropertyName(name),
                    JsonValue = parameter.DefaultValue.Value
                };
            }
        }
        
        private static void CacheResult(string resultJson)
        {
            var cachePath = $"{Application.dataPath}/Integrations/Firebase/Tools/Cache/remote-template.json";
            File.WriteAllText(cachePath, resultJson);
        }
    }
}