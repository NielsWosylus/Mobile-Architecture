﻿namespace Integrations.Firebase.Tools
{
    internal static class Utility
    {
        public static string GetFeatureName(string fullParameterName)
        {
            return fullParameterName.Contains("_") ? fullParameterName.Split("_")[0] : "";
        }
        
        public static string GetPropertyName(string fullParameterName)
        {
            return fullParameterName.Contains("_") ? fullParameterName.Split("_")[1] : fullParameterName;
        }
    }
}