﻿using Newtonsoft.Json;

namespace Integrations.Firebase.Tools.DTOs
{
    public class UpdateUser
    {
        [JsonProperty("email")]
        public string Email { get; set; }
    }
}