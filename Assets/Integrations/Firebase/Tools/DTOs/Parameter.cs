﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Integrations.Firebase.Tools.DTOs
{
    public class Parameter
    {
        [JsonProperty("defaultValue")]
        public ParameterValue DefaultValue { get; set; }

        [JsonProperty("conditionalValues")]
        public Dictionary<string, ParameterValue> ConditionalValues { get; set; }

        [JsonProperty("valueType")]
        public string ValueType { get; set; }
    }
}