﻿using Newtonsoft.Json;

namespace Integrations.Firebase.Tools.DTOs
{
    public class Condition
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("expression")]
        public string Expression { get; set; }

        [JsonProperty("tagColor")]
        public string TagColor { get; set; }
    }
}