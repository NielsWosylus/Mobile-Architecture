﻿using System;
using Newtonsoft.Json;

namespace Integrations.Firebase.Tools.DTOs
{
    public class Version
    {
        [JsonProperty("versionNumber")]
        public string VersionNumber { get; set; }

        [JsonProperty("updateTime")]
        public DateTime UpdateTime { get; set; }

        [JsonProperty("updateUser")]
        public UpdateUser UpdateUser { get; set; }

        [JsonProperty("updateOrigin")]
        public string UpdateOrigin { get; set; }

        [JsonProperty("updateType")]
        public string UpdateType { get; set; }
    }
}