﻿using Newtonsoft.Json;

namespace Integrations.Firebase.Tools.DTOs
{
    public class ParameterValue
    {
        [JsonProperty("value")]
        public string Value { get; set; }
        
        [JsonProperty("personalizationValue")]
        public PersonalizationValue PersonalizationValue { get; set; }
    }
}