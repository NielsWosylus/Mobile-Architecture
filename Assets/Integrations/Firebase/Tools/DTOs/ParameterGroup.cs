﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Integrations.Firebase.Tools.DTOs
{
    public class ParameterGroup
    {
        [JsonProperty("parameters")] 
        public Dictionary<string, Parameter> Parameters = new();
    }
}