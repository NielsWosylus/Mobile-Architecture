﻿using Newtonsoft.Json;

namespace Integrations.Firebase.Tools.DTOs
{
    public class PersonalizationValue
    {
        [JsonProperty("personalizationId")]
        public string PersonalizationId { get; set; }
    }
}