﻿using System.Collections.Generic;
using Infrastructure.Configuration;
using Newtonsoft.Json;

namespace Integrations.Firebase.Tools.DTOs
{
    public class Template
    {
        [JsonProperty("conditions")]
        public List<Condition> Conditions { get; set; }

        [JsonProperty("parameters")]
        public Dictionary<string, Parameter> Parameters { get; set; }

        [JsonProperty("version")]
        public Version Version { get; set; }

        [JsonProperty("parameterGroups")]
        public Dictionary<string, ParameterGroup> ParameterGroups { get; set; } = new();
        
        /// <summary>
        /// Returns all parameters from defaults and groups whose name match a prefix. Prefix is stripped. 
        /// </summary>
        public IEnumerable<(string name, Parameter parameter)> GetAllParameters()
        {
            var all = new List<(string name, Parameter parameter)>();
            if (Parameters != null)
            {
                foreach (var (parameterName, parameter) in Parameters)
                {
                    all.Add((parameterName, parameter));
                }
            }

            if(ParameterGroups == null)
                return all;

            foreach (var group in ParameterGroups.Values)
            {
                if (group.Parameters == null)
                    continue;
                
                foreach (var (parameterName, parameter) in group.Parameters)
                {
                    all.Add((parameterName, parameter));
                }
            }

            return all;
        }

        /// <summary>
        /// Puts parameters into fitting groups
        /// </summary>
        public void Organise()
        {
            var parameters = GetAllParameters();
            Parameters = new Dictionary<string, Parameter>();
            ParameterGroups = new Dictionary<string, ParameterGroup>();

            foreach (var (name, parameter) in parameters)
            {
                var featureName = Utility.GetFeatureName(name);
                if (string.IsNullOrEmpty(featureName))
                {
                    Parameters.Add(name, parameter);
                }
                else
                {
                    ParameterGroups.TryAdd(featureName, new ParameterGroup());
                    ParameterGroups[featureName].Parameters.Add(name, parameter);
                }
            }
        }
    }
}