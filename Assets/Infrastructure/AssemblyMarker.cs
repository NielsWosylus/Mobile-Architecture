using System.Runtime.CompilerServices;
using Infrastructure.Injection;

[assembly: ScanForFeatures]
[assembly: InternalsVisibleTo("Infrastructure.Tools")]
[assembly: InternalsVisibleTo("Infrastructure.Tests")]
 
 namespace Infrastructure
 {
     public interface IAssemblyMarker { }
 }