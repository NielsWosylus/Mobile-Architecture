﻿using UnityEngine;

namespace Infrastructure.Services
{
    public abstract class Controller<T> : MonoBehaviour where T : Service
    {
        protected T Service { get; private set; }
        
        private void InjectService(T service)
        {
            Service = service;
        }
    }
}