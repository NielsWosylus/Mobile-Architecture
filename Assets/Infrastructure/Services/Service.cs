using Infrastructure.Configuration;
using Infrastructure.Logging;
using Infrastructure.Persistence;
using Zenject;

namespace Infrastructure.Services
{
    /// <summary>
    /// A service with a dedicated logger
    /// </summary>
    public abstract class Service
    {
        protected ILogger Logger { get; private set; } = new NullLogger();

        [Inject]
        private void InjectLogger(ILogger logger)
        {
            Logger = logger;
        }
    }
    
    /// <summary>
    /// A service that is both persistent and configurable
    /// </summary>
    /// <typeparam name="TConfig">Configuration type</typeparam>
    /// <typeparam name="TSave">Persistent data type</typeparam>
    public abstract class Service<TConfig, TSave> : Service, IConfigurable<TConfig>, IPersistent<TSave> where TConfig : Config
    {
        protected TConfig Config { get; set; }
        protected TSave Data { get; set; }
        
        public virtual void Configure(TConfig config)
        {
            Config = config;
        }

        public virtual TSave Save()
        {
            return Data;
        }

        public virtual void Load(TSave data)
        {
            Data = data;
        }
    }

    /// <summary>
    /// A service that is configurable
    /// </summary>
    /// <typeparam name="TConfig">Configuration type</typeparam>
    public abstract class ConfigurableService<TConfig> : Service, IConfigurable<TConfig> where TConfig : Config
    {
        protected TConfig Config { get; set; }
        
        public virtual void Configure(TConfig config)
        {
            Config = config;
        }
    }

    /// <summary>
    /// A service that is persistent
    /// </summary>
    /// <typeparam name="TSave">Persistent data type</typeparam>
    public abstract class PersistentService<TSave> : Service, IPersistent<TSave>
    {
        protected TSave Data { get; set; }
        
        public virtual TSave Save()
        {
            return Data;
        }

        public virtual void Load(TSave data)
        {
            Data = data;
        }
    }
}