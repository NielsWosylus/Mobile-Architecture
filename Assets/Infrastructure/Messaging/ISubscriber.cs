namespace Infrastructure.Messaging
{
    public interface ISubscriber<in T>
    {
        void OnSignal(T message);
    }
    
    public interface ISubscriber<in T1, in T2>
    {
        void OnSignal(T1 message);
        void OnSignal(T2 message);
    }
    
    public interface ISubscriber<in T1, in T2, in T3>
    {
        void OnSignal(T1 message);
        void OnSignal(T2 message);
        void OnSignal(T3 message);
    }
}