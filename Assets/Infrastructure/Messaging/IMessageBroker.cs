﻿using System;

namespace Infrastructure.Messaging
{
    public delegate void EventHandler<in T>(T message);
    
    public interface IMessageBroker
    {
        void Publish<T>(T message);
        IDisposable Subscribe<T>(EventHandler<T> handler);
        IDisposable Subscribe<T>(ISubscriber<T> subscriber);
        IDisposable Subscribe<T1, T2>(ISubscriber<T1, T2> subscriber);
        IDisposable Subscribe<T1, T2, T3>(ISubscriber<T1, T2, T3> subscriber);
    }
}