﻿using System;

namespace Infrastructure.Logging
{
    public class NullLogger : ILogger
    {
        public void Message(string message) { }

        public void Warning(string message) { }

        public void Warning(string message, Exception exception) { }

        public void Error(string message, Exception exception) { }

        public void Exception(Exception exception) { }
    }
}