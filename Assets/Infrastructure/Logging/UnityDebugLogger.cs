using System;
using UnityEngine;

namespace Infrastructure.Logging
{
    public class UnityDebugLogger : ILogger
    {
        public void Message(string message)
        {
            Debug.Log(message);
        }

        public void Warning(string message)
        {
            Debug.LogWarning(message);
        }

        public void Warning(string message, Exception exception)
        {
            Debug.LogWarning($"{message}:\n{exception}");
        }

        public void Error(string message, Exception exception)
        {
            Debug.LogError($"{message}:\n{exception}");
        }

        public void Error(string message)
        {
            Debug.LogError(message);
        }

        public void Exception(Exception exception)
        {
            Debug.LogException(exception);
        }
    }
}