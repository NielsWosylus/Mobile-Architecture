using System;

namespace Infrastructure.Logging
{
    public interface ILogger
    {
        void Message(string message);
        void Warning(string message);
        void Warning(string message, Exception exception);
        void Error(string message, Exception exception);
        void Exception(Exception exception);
    }
}