﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class PrefabGetter
{
    public static List<T> GetAll<T>() where T : Component
    {
        var prefabs = new List<T>(1000);
        var prefabGuids = AssetDatabase.FindAssets("t:prefab");
        foreach (var prefabGuid in prefabGuids)
        {
            var prefabPath = AssetDatabase.GUIDToAssetPath(prefabGuid);
            var level = AssetDatabase.LoadAssetAtPath<T>(prefabPath);
            if (level != null) prefabs.Add(level);
        }

        return prefabs;
    }
    
    public static List<T> GetAll<T>(params string[] folderPaths) where T : Component
    {
        var prefabs = new List<T>(1000);
        var prefabGuids = AssetDatabase.FindAssets("t:prefab", folderPaths);
        foreach (var prefabGuid in prefabGuids)
        {
            var prefabPath = AssetDatabase.GUIDToAssetPath(prefabGuid);
            var level = AssetDatabase.LoadAssetAtPath<T>(prefabPath);
            if (level != null) prefabs.Add(level);
        }

        return prefabs;
    }
}

#endif