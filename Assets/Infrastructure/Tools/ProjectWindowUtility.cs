﻿using System.IO;
using UnityEditor;

namespace Infrastructure.Tools
{
    public class ProjectWindowUtility
    {
        public static string GetSelectedPathOrFallback(string fallback)
        {
            string path = fallback;
		
            foreach (UnityEngine.Object obj in Selection.GetFiltered(typeof(UnityEngine.Object), SelectionMode.Assets))
            {
                path = AssetDatabase.GetAssetPath(obj);
                if ( !string.IsNullOrEmpty(path) && File.Exists(path) ) 
                {
                    path = Path.GetDirectoryName(path);
                    break;
                }
            }
            
            return path;
        }
        
        public static void SelectFolder(string folderPath)
        {
            var obj = AssetDatabase.LoadAssetAtPath(folderPath, typeof(UnityEngine.Object));
            Selection.activeObject = obj;
            EditorGUIUtility.PingObject(obj);
        }
    }
}