using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public static class AssetGetter
{
    public static IEnumerable<T> GetAll<T>() where T : ScriptableObject
    {
        var assets = new List<T>(1000);
        var guids = AssetDatabase.FindAssets($"t:{typeof(T)}");
        foreach (var guid in guids)
        {
            var path = AssetDatabase.GUIDToAssetPath(guid);
            var asset = AssetDatabase.LoadAssetAtPath<T>(path);
            if (asset != null) assets.Add(asset);
        }

        return assets;
    }
}