﻿using System.IO;
using UnityEngine;

namespace Infrastructure.Tools.FeatureCreation
{
    internal struct FeatureInfo
    {
        public string FeatureName;
        public string FeatureNameSpace;
        
        public string FolderPath;
        public string ParentFolderPath;
        
        public string DiskFolderPath
        {
            get
            {
                var pathExtension = FolderPath.Remove(0, "Assets".Length);
                return Path.Join(Application.dataPath, pathExtension);
            }
        }

        public static FeatureInfo From(NewFeatureWizard wizard)
        {
            var folderPath = Path.Join(wizard.ParentFolder, wizard.FeatureName);
            var nameSpace = folderPath
                .Remove(0, "Assets/".Length)
                .Replace('\\', '.')
                .Replace('/', '.');
            
            return new FeatureInfo
            {
                FeatureName = wizard.FeatureName,
                FeatureNameSpace = nameSpace,
                FolderPath = folderPath
            };
        }
    }
}