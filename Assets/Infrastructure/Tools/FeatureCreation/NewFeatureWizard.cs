﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Infrastructure.Tools.FeatureCreation
{
    public class NewFeatureWizard : ScriptableWizard
    {
        [Header("Info")]
        public string FeatureName;
        public string ParentFolder;
        
        [Header("Settings")]
        public bool Configuration = true;
        public bool Persistence = true;
        public bool Messaging = true;
        
        [Header("Utility")]
        public bool Tests = true;
        public bool Tools = false;

        [MenuItem("Assets/Create/New Feature", priority = -9999)]
        public static void Open()
        {
            var wizard = DisplayWizard<NewFeatureWizard>("Create Feature", "Create", "Cancel");
            wizard.ParentFolder = ProjectWindowUtility.GetSelectedPathOrFallback("Assets/Features");
        }
        
        private void OnWizardOtherButton() => Close();

        private void OnWizardCreate()
        {
            FeatureName = GetValidFeatureName(FeatureName);
            if (string.IsNullOrEmpty(FeatureName))
                throw new Exception("Please supply a valid feature name");

            var info = FeatureInfo.From(this);
            CreateFeatureAssets(info);
            AssetDatabase.Refresh();
            ProjectWindowUtility.SelectFolder(info.FolderPath);
        }
        
        private void CreateFeatureAssets(FeatureInfo info)
        {
            AssetDatabase.CreateFolder(ParentFolder, FeatureName);
            
            CreateServiceClass(info);
            if(Configuration) CreateClass(info, "Config");
            if(Persistence) CreateClass(info, "Save");
            if(Messaging) CreateClass(info, "Signals");

            if (Tools)
            {
                AssetDatabase.CreateFolder(info.FolderPath, "Tools");
                //TODO: Get assembly asset GUID -> Create assembly reference
            }
            
            if (Tests)
            {
                AssetDatabase.CreateFolder(info.FolderPath, "Tests");
                //TODO: Get assembly asset GUID -> Create assembly reference -> Create test fixture
            }
        }

        private void CreateServiceClass(FeatureInfo info)
        {
            var templateVariant = (Configuration, Persistence) switch
            {
                (true, true) => "-Full",
                (true, false) => "-ConfigOnly",
                (false, true) => "-PersistOnly",
                _ => "-Simple"
            };

            CreateClass(info, "Service", templateVariant);
        }

        private static void CreateClass(FeatureInfo info, string subtype, string templateVariant = "")
        {
            var text = ReadTemplate($"{subtype}Template{templateVariant}.txt")
                .Replace("[FEATURE_NAME]", info.FeatureName)
                .Replace("[FEATURE_NAMESPACE]", info.FeatureNameSpace);
            
            var path = Path.Join(info.DiskFolderPath, $"{info.FeatureName}{subtype}.cs");
            File.WriteAllText(path, text);
        }
        
        private static string ReadTemplate(string fileName)
        {
            var folderPath = Path.Join(Application.dataPath, "Infrastructure/Tools/FeatureCreation/Templates");
            var filePath = Path.Join(folderPath, fileName);
            return File.ReadAllText(filePath);
        }
        
        private static string GetValidFeatureName(string featureName)
        {
            if (string.IsNullOrEmpty(featureName))
                return "";
            
            featureName = featureName.Replace(" ", "");
            var firstChar = featureName[0].ToString().ToUpper();
            featureName = $"{firstChar}{featureName.Remove(0, 1)}";

            return featureName;
        }
    }
}