﻿using [FEATURE_NAMESPACE];
using Services;

namespace [FEATURE_NAMESPACE]
{
    public interface I[FEATURE_NAME]Service
    {
        
    }
    
    public class [FEATURE_NAME]Service : ConfigurableService<[FEATURE_NAME]Config>, I[FEATURE_NAME]Service
    {
        
    }
}