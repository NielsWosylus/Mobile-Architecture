﻿using System;
using Configuration;
using Configuration.Attributes;
using UnityEngine;

namespace [FEATURE_NAMESPACE]
{
    [Serializable, Configuration]
    public class [FEATURE_NAME]Config : Config
    {
        
        
        public override void Validate()
        {

        }
    }
}