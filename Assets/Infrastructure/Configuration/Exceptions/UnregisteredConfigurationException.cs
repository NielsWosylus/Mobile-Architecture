using System;

namespace Infrastructure.Configuration.Exceptions
{
    public class UnregisteredConfigurationException : Exception
    {
        public UnregisteredConfigurationException() { }
        
        public UnregisteredConfigurationException(string message) : base(message) { }

        public UnregisteredConfigurationException(string message, Exception inner) : base(message, inner) { }
    }
}