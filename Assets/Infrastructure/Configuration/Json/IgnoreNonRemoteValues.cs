using System.Linq;
using System.Reflection;
using Infrastructure.Configuration.Attributes;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Infrastructure.Configuration.Json
{
    public class IgnoreNonRemoteValues : DefaultContractResolver
    {
        protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
        {
            var property = base.CreateProperty(member, memberSerialization);
            var ignore = property.AttributeProvider.GetAttributes(false)
                .FirstOrDefault(x => x is RemoteValueAttribute) == null;
            property.Ignored = ignore;
            return property;
        }
    }
}