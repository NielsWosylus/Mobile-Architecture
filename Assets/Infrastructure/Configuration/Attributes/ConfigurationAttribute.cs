using System;

namespace Infrastructure.Configuration.Attributes
{
    /// <summary>
    /// Marks a class as a configuration which will show up in the overview
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ConfigurationAttribute : Attribute
    {
        public ConfigurationAttribute()
        {
            
        }

        public ConfigurationAttribute(string modelName)
        {
            ModelName = modelName;
        }

        public readonly string ModelName;
    }
}