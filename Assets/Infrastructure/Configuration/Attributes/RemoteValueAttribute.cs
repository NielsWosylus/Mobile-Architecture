using System;

namespace Infrastructure.Configuration.Attributes
{
    /// <summary>
    /// Apply to a field to fetch the value remotely if possible
    /// </summary>
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class RemoteValueAttribute : Attribute
    {
        
    }
}