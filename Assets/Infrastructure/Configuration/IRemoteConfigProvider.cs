using Cysharp.Threading.Tasks;

namespace Infrastructure.Configuration
{
    public interface IRemoteConfigProvider
    {
        UniTask FetchAll();
        string GetJsonValue(string featureName, string propertyName);
    }
}