﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Infrastructure.Configuration.Attributes;

namespace Infrastructure.Configuration
{
    public abstract class Config
    {
        public virtual void Validate() { }

        internal Config Clone()
        {
            return (Config)MemberwiseClone();
        }

        internal string GetFeatureName()
        {
            var type = GetType();
            var attribute = type.GetCustomAttributes(typeof(ConfigurationAttribute)).FirstOrDefault();
            if (attribute == null) 
                return InferFeatureNameFromTypeName(type.Name);
            
            var cast = (ConfigurationAttribute) attribute;
            var identifier = cast.ModelName;
            return string.IsNullOrEmpty(identifier) ? InferFeatureNameFromTypeName(type.Name) : identifier;
        }
        
        private static string InferFeatureNameFromTypeName(string typeName)
        {
            return typeName.Replace("Configuration", "").Replace("Config", "");
        }

        internal IEnumerable<IReflectedProperty> GetRemoteProperties()
        {
            var members = GetType().GetMembers();
            foreach (var member in members)
            {
                if (!member.IsMarkedAsRemote())
                    continue;

                if (member is PropertyInfo property)
                    yield return new ReflectedProperty(this, property);
                else if (member is FieldInfo field)
                    yield return new ReflectedField(this, field);
            }
        }
    }
}