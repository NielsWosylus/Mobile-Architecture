namespace Infrastructure.Configuration
{
    public interface IConfigurable<in T> where T : Config
    {
        void Configure(T config);
    }
}