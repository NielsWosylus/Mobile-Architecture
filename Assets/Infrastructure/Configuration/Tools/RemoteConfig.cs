﻿using System.Collections.Generic;
using Infrastructure.Configuration;

namespace Configuration.Tools
{
    public class RemoteConfig
    {
        public readonly string FeatureName;
        public readonly Config Config;
        public readonly IEnumerable<IReflectedProperty> LocalProperties;
        public readonly IEnumerable<RemoteProperty> RemoteProperties;
    }
}