﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using Infrastructure.Configuration;
using Infrastructure.Configuration.Containers;
using UnityEditor;

namespace Configuration.Tools
{
    public static class ToolsService
    {
        public static event Action UpdatedRemotes;
        public static RemotePropertyCollection RemoteProperties { get; private set; } = new();
        private static IRemoteConfigBackend Backend => RemoteConfigBackend.Instance;

        public static IEnumerable<ConfigAsset> GetAllConfigAssets()
        {
            var container = AssetGetter.GetAll<ConfigContainer>().First();
            return container._configs;
        }
        
        public static async UniTask RefreshRemotes()
        {
            var remotes = await Backend.FetchAll();
            RemoteProperties = new RemotePropertyCollection(remotes);
            UpdatedRemotes?.Invoke();
        }
        
        public static void CopyDefaultsInto(ConfigAsset asset)
        {
            Undo.RecordObject(asset, "pull default config");
            var config = asset._value;
            var featureName = config.GetFeatureName();
            var locals = config.GetRemoteProperties();
            var remotes = RemoteProperties;

            foreach (var property in locals)
            {
                var propertyName = property.PropertyName;
                if (!remotes.Contains(featureName, propertyName))
                    continue;

                var remote = remotes.Get(featureName, propertyName);
                var jsonValue = remote.JsonValue;
                property.JsonValue = jsonValue;
            }
            
            EditorUtility.SetDirty(asset);
            AssetDatabase.SaveAssetIfDirty(asset);
        }
        
        public static async UniTask PushMissingProperties(ConfigAsset asset)
        {
            var config = asset._value;
            var featureName = config.GetFeatureName();
            var locals = config.GetRemoteProperties();
            var remotes = RemoteProperties;
            var missing = new List<RemoteProperty>();

            foreach (var property in locals)
            {
                var propertyName = property.PropertyName;
                if (remotes.Contains(featureName, propertyName)) 
                    continue;
                
                missing.Add(new RemoteProperty(property));
            }

            await Backend.Update(missing);
            await RefreshRemotes();
        }
    }
}