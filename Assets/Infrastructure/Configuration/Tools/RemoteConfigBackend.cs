﻿using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Infrastructure.Configuration;

namespace Configuration.Tools
{
    public interface IRemoteConfigBackend
    {
        UniTask<IEnumerable<RemoteProperty>> FetchAll();
        UniTask Update(IEnumerable<RemoteProperty> properties);
    }

    public static class RemoteConfigBackend
    {
        public static IRemoteConfigBackend Instance { get; private set; }

        public static void RegisterInstance(IRemoteConfigBackend instance)
        {
            Instance = instance;
        }
    }
}