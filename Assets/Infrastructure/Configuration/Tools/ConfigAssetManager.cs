﻿using System;
using System.Collections.Generic;
using System.Linq;
using Infrastructure.Configuration;
using Infrastructure.Configuration.Attributes;
using Infrastructure.Configuration.Containers;
using Infrastructure.Injection;
using UnityEditor;
using UnityEngine;

namespace Configuration.Tools
{
    public static class ConfigAssetManager
    {
        [InitializeOnLoadMethod]
        private static void EnsureAllConfigTypesExist()
        {
            var container = GetContainer();
            var dirty = false;
            var types = GetAllConfigTypes().ToArray();
            var configs = AssetGetter.GetAll<ConfigAsset>().ToArray();
            var existingTypes =
                (from cfg in configs
                    select cfg.ValueType).ToArray();

            foreach (var type in types)
            {
                if (existingTypes.Contains(type))
                    continue;
                
                CreateConfigAsset(type);
                dirty = true;
            }

            foreach (var config in configs)
            {
                if (types.Contains(config.ValueType))
                    continue;

                var path = AssetDatabase.GetAssetPath(config);
                AssetDatabase.DeleteAsset(path);
                dirty = true;
            }

            UpdateContainer(container);
            
            if (!dirty) return;
            AssetDatabase.Refresh();
        }

        private static void CreateConfigAsset(Type type)
        {
            var asset = ScriptableObject.CreateInstance<ConfigAsset>();
            var value = (Config)Activator.CreateInstance(type);
            asset._value = value;
            asset.name = $"{value.GetFeatureName()} Config";
            AssetDatabase.CreateAsset(asset, $"Assets/Configuration/{asset.name}.asset");
        }

        private static void UpdateContainer(ConfigContainer container)
        {
            container._configs = AssetGetter.GetAll<ConfigAsset>().ToList();
            EditorUtility.SetDirty(container);
            AssetDatabase.SaveAssetIfDirty(container);
        }

        private static ConfigContainer GetContainer()
        {
            var container = AssetGetter.GetAll<ConfigContainer>().FirstOrDefault();
            if (container != null) return container;
            
            container = ScriptableObject.CreateInstance<ConfigContainer>();
            container.name = "Config Collection";
            AssetDatabase.CreateAsset(container, $"Assets/Configuration/{container.name}.asset");
            UpdateContainer(container);
            
            return container;
        }

        private static IEnumerable<Type> GetAllConfigTypes()
        {
            var assemblies =
                from assembly in AppDomain.CurrentDomain.GetAssemblies()
                let attributes = assembly.GetCustomAttributes(typeof(ScanForFeaturesAttribute), true)
                where attributes is { Length: > 0 }
                select assembly;

            var result =
                from assembly in assemblies
                from type in assembly.GetTypes()
                let attributes = type.GetCustomAttributes(typeof(ConfigurationAttribute), true)
                where attributes is { Length: > 0 }
                select type;

            return result;
        }
    }
}