﻿using System;
using System.Linq;
using Infrastructure.Configuration;
using Sirenix.Utilities.Editor;
using UnityEditor;

namespace Configuration.Tools
{
    public class PropertyTable
    {
        private readonly RemotePropertyCollection _remoteProperties;
        private GUITable _guiTable;
        private TableRow[] _rows;
        private Config _config;

        public PropertyTable(RemotePropertyCollection remoteProperties)
        {
            _remoteProperties = remoteProperties;
            _guiTable = BuildTable();
        }

        public void Draw(Config config)
        {
            if (config != _config)
            {
                _config = config;
                _guiTable = BuildTable();
            }
            
            _guiTable.DrawTable();
        }
        
        private GUITable BuildTable()
        {
            var columns = new[]
            {
                BuildColumn("Property", entry => entry.PropertyName),
                BuildColumn("LOCAL (default)", entry => entry.LocalDefault),
                BuildColumn("REMOTE (default)", entry => entry.RemoteDefault)
            };

            _rows = BuildRows();
            return GUITable.Create(_rows.Length, "Remote Values", columns);
        }

        private GUITableColumn BuildColumn(string title, Func<TableRow, string> cellValue)
        {
            return new GUITableColumn
            {
                ColumnTitle = title, 
                MinWidth = 100,
                OnGUI = (rect, i) =>
                {
                    EditorGUI.LabelField(rect, cellValue.Invoke(_rows[i]));
                }
            };
        }
        
        private TableRow[] BuildRows()
        {
            if (_config == null)
                return Array.Empty<TableRow>();
            
            var locals = _config.GetRemoteProperties();
            return locals.Select(BuildRow).ToArray();
        }

        private TableRow BuildRow(IReflectedProperty property)
        {
            var featureName = property.FeatureName;
            var propertyName = property.PropertyName;
            if (!_remoteProperties.Contains(featureName, propertyName))
                return new TableRow(property, new RemoteProperty());
            
            var remote = _remoteProperties.Get(featureName, propertyName);
            return new TableRow(property, remote);
        }
        
        private readonly struct TableRow
        {
            public TableRow(IReflectedProperty localProperty, RemoteProperty remoteProperty)
            {
                LocalProperty = localProperty;
                RemoteProperty = remoteProperty;
            }

            private IReflectedProperty LocalProperty { get; }
            private RemoteProperty RemoteProperty { get; }

            public string PropertyName => LocalProperty.FeatureName;
            public string LocalDefault => LocalProperty.JsonValue;
            public string RemoteDefault => RemoteProperty.JsonValue;
        }
    }
}