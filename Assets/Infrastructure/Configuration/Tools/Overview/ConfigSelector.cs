﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure.Configuration.Containers;
using UnityEngine;

namespace Configuration.Tools
{
    public class ConfigSelector
    {
        public ConfigAsset Selected => _configs[_selectedIndex];
        
        private readonly ConfigAsset[] _configs;
        private readonly string[] _configNames;
        private readonly int _width;
        private int _selectedIndex;
        
        public ConfigSelector(IEnumerable<ConfigAsset> configs, int width)
        {
            _configs = configs.ToArray();
            _configNames = _configs.Select(c => c.FeatureName).ToArray();
            _width = width;
        }
        
        public void Draw()
        {
            _selectedIndex = GUILayout.SelectionGrid(_selectedIndex, _configNames, 1, 
                GUILayout.MinWidth(_width), 
                GUILayout.MaxWidth(_width));
        }
    }
}