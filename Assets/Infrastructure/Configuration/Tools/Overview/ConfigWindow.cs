using Cysharp.Threading.Tasks;
using Infrastructure.Configuration.Containers;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace Configuration.Tools
{
    internal class ConfigWindow : OdinEditorWindow
    {
        [MenuItem("Config/Overview")]
        private static void ShowWindow()
        {
            var window = GetWindow<ConfigWindow>();
            window.titleContent = new GUIContent("Configuration");
            window.Show();
        }

        private ConfigAsset SelectedAsset => _configSelector.Selected;
        private ConfigSelector _configSelector;
        private PropertyTable _propertyTable;

        protected override void Initialize()
        {
            ToolsService.UpdatedRemotes -= UpdateTable;
            ToolsService.UpdatedRemotes += UpdateTable;
            
            var allConfigs = ToolsService.GetAllConfigAssets();
            _configSelector = new ConfigSelector(allConfigs, 150);
            _propertyTable = new PropertyTable(ToolsService.RemoteProperties);
        }

        private void UpdateTable()
        {
            _propertyTable = new PropertyTable(ToolsService.RemoteProperties);
        }

        protected override void OnGUI()
        {
            if(_configSelector == null)
                Initialize();
            
            EditorGUILayout.BeginHorizontal();
                _configSelector.Draw();
                DrawAssetInspector();
                EditorGUILayout.BeginVertical();
                    DrawControlButtons();
                    _propertyTable.Draw(SelectedAsset._value);
                    EditorGUILayout.EndVertical();
            EditorGUILayout.EndHorizontal();
        }

        private void DrawAssetInspector()
        {
            EditorGUILayout.BeginHorizontal(GUILayout.MinWidth(400), GUILayout.MaxWidth(400));
            base.OnGUI();
            EditorGUILayout.EndHorizontal();
        }

        private void DrawControlButtons()
        {
            if (GUILayout.Button("Fetch all"))
                ToolsService.RefreshRemotes().Forget();

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("<-- Copy defaults"))
                ToolsService.CopyDefaultsInto(SelectedAsset);

            if (GUILayout.Button("Push missing -->"))
                ToolsService.PushMissingProperties(SelectedAsset).Forget();

            EditorGUILayout.EndHorizontal();
        }

        protected override object GetTarget()
        {
            return SelectedAsset;
        }
    }
}