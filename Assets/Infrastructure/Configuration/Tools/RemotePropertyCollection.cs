﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure.Configuration;

namespace Configuration.Tools
{
    public class RemotePropertyCollection
    {
        public IEnumerable<RemoteProperty> Properties => _properties;
        private readonly List<RemoteProperty> _properties = new();

        public RemotePropertyCollection() { }
        
        public RemotePropertyCollection(IEnumerable<RemoteProperty> properties)
        {
            foreach (var property in properties)
            {
                Add(property);
            }
        }

        public void Add(RemoteProperty property)
        {
            _properties.Add(property);
        }

        public IEnumerable<RemoteProperty> Get(string featureName)
        {
            return _properties.Where(p => p.FeatureName == featureName);
        }

        public RemoteProperty Get(string featureName, string propertyName)
        {
            return _properties.First(p => p.FeatureName == featureName && p.PropertyName == propertyName);
        }

        public bool Contains(string featureName, string propertyName)
        {
            return _properties.Count(p => p.FeatureName == featureName && p.PropertyName == propertyName) > 0;
        }
    }
}