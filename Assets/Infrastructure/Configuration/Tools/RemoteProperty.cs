﻿using System.Linq;
using System.Reflection;
using Newtonsoft.Json;

namespace Infrastructure.Configuration
{
    public struct RemoteProperty
    {
        public string FeatureName;
        public string PropertyName;
        public string JsonValue;

        public RemoteProperty(IReflectedProperty property)
        {
            FeatureName = property.FeatureName;
            PropertyName = property.PropertyName;
            JsonValue = property.JsonValue;
        }
    }
}