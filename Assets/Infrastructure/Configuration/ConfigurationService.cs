using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using Infrastructure.Configuration.Exceptions;
using ILogger = Infrastructure.Logging.ILogger;

namespace Infrastructure.Configuration
{
    public class ConfigurationService
    {
        private readonly IRemoteConfigProvider _remoteConfigProvider;
        private readonly ILogger _logger;

        private readonly Dictionary<Type, object> _default = new();
        private readonly Dictionary<Type, object> _merged = new();

        public ConfigurationService(IRemoteConfigProvider remoteConfigProvider, ILogger logger)
        {
            _remoteConfigProvider = remoteConfigProvider;
            _logger = logger;
        }
        
        public void RegisterDefault<T>(T configuration) where T : Config
        {
            _default.Add(typeof(T), configuration);
        }

        public async UniTaskVoid FetchRemoteValues()
        {
            await _remoteConfigProvider.FetchAll();
        }

        public T Get<T>() where T : Config
        {
            var type = typeof(T);
            
            if (_merged.TryGetValue(type, out var result))
                return (T)result;

            if (!_default.ContainsKey(type))
                throw new UnregisteredConfigurationException($"Unregistered configuration of type {type.Name}. Please register a default.");

            var defaultConfig = (T)_default[type];

            try
            {
                return BuildMergedFrom(defaultConfig, _remoteConfigProvider);
            }
            catch (Exception e)
            {
                _logger.Warning($"Failed to apply remote configuration for {type.Name}, using default", e);
                return defaultConfig;
            }
        }

        private T BuildMergedFrom<T>(T original, IRemoteConfigProvider remoteProvider) where T : Config
        {
            var merged = Utility.BuildMergedFrom(original, remoteProvider);
            merged.Validate();
            _merged.Add(typeof(T), merged);
            return merged;
        }
    }
}