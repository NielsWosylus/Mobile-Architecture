﻿using System;
using System.Reflection;
using Newtonsoft.Json;

namespace Infrastructure.Configuration
{
    public interface IReflectedProperty
    {
        string FeatureName { get; }
        string PropertyName { get; }
        Type ValueType { get; }
        object Value { get; set; }
        string JsonValue { get; set; }
    }
    
    public class ReflectedProperty : IReflectedProperty
    {
        public string FeatureName { get; }
        public string PropertyName { get; }
        public Type ValueType { get; }
        
        private readonly Config _config;
        private readonly PropertyInfo _property;

        public object Value
        {
            get => _property.GetValue(_config);
            set => _property.SetValue(_config, value);
        }

        public string JsonValue
        {
            get => JsonConvert.SerializeObject(Value);
            set => Value = JsonConvert.DeserializeObject(value, ValueType);
        }

        public ReflectedProperty(Config config, PropertyInfo property)
        {
            _property = property;
            _config = config;
            ValueType = property.PropertyType;
            PropertyName = property.Name;
            FeatureName = config.GetFeatureName();
        }
    }
    
    public class ReflectedField : IReflectedProperty
    {
        public string FeatureName { get; }
        public string PropertyName { get; }
        public Type ValueType { get; }
        
        private readonly Config _config;
        private readonly FieldInfo _field;

        public object Value
        {
            get => _field.GetValue(_config);
            set => _field.SetValue(_config, value);
        }

        public string JsonValue
        {
            get => JsonConvert.SerializeObject(Value);
            set => Value = JsonConvert.DeserializeObject(value, ValueType);
        }

        public ReflectedField(Config config, FieldInfo field)
        {
            _field = field;
            _config = config;
            ValueType = field.FieldType;
            PropertyName = field.Name;
            FeatureName = config.GetFeatureName();
        }
    }
}