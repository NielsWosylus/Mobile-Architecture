using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Infrastructure.Configuration.Features
{
    public interface IFeatureCollection
    {
        bool IsEnabled(string feature);
    }
    
    [CreateAssetMenu]
    public class SerializedFeatureCollection : SerializedScriptableObject, IFeatureCollection
    {
        [SerializeField, InlineProperty] 
        private Dictionary<string, FeatureStatus> _features = new Dictionary<string, FeatureStatus>();

        public bool IsEnabled(string feature)
        {
            return _features[feature] == FeatureStatus.Enabled;
        }
    }
}