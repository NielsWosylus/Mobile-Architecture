namespace Infrastructure.Configuration.Features
{
    public enum FeatureStatus
    {
        /// <summary>
        /// The feature is available to the end user
        /// </summary>
        Enabled,
        
        /// <summary>
        /// The feature is hidden from the end user
        /// </summary>
        Disabled,
        
        /// <summary>
        /// The feature is hidden from the end user, and cannot be enabled remotely for this build.
        /// Mark features that are not yet ready for use as UnderConstruction to prevent accidents.
        /// </summary>
        UnderConstruction
    }
}