using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace Infrastructure.Configuration.Features
{
    public class FeatureToggle : MonoBehaviour
    {
        [ValueDropdown(nameof(GetValidFeatureNames))]
        [SerializeField] private string _feature = default;
        [SerializeField] private UnityEvent _onFeatureEnabled = default;
        [SerializeField] private UnityEvent _onFeatureDisabled = default;

        private void Awake()
        {
            // if(Feature.Of(_feature).IsEnabled)
            //     _onFeatureEnabled.Invoke();
            // else _onFeatureDisabled.Invoke();
        }
    
        private IEnumerable<string> GetValidFeatureNames()
        {
            //TODO: get valid features at edit time
            return new[] {"Smart new camera", "Infinite mode", "PVP ogre mode"};
        }
    }
}