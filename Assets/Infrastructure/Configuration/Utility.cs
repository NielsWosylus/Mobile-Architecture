using System;
using System.Reflection;
using Infrastructure.Configuration.Attributes;

namespace Infrastructure.Configuration
{
    internal static class Utility
    {
        public static T BuildMergedFrom<T>(T defaultConfig, IRemoteConfigProvider remoteProvider) where T : Config
        {
            var merged = defaultConfig.MakeCopy();
            var remoteProperties = merged.GetRemoteProperties();

            foreach (var property in remoteProperties)
            {
                var jsonValue = remoteProvider.GetJsonValue(property.FeatureName, property.PropertyName);
                property.JsonValue = jsonValue;
            }

            return merged;
        }
    }
    
    internal static class Extensions
    {
        public static bool IsMarkedAsRemote(this MemberInfo member)
        {
            return Attribute.IsDefined(member, typeof(RemoteValueAttribute));
        }

        public static T MakeCopy<T>(this T config) where T : Config
        {
            return (T)config.Clone();
        }
    }
}