using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Infrastructure.Configuration.Containers
{
    public class ConfigAsset : SerializedScriptableObject
    {
        [ShowInInspector, PropertyOrder(-99999)]
        public string FeatureName => _value.GetFeatureName();
        public Type ValueType => _value.GetType();
        
        [SerializeField, InlineProperty, HideLabel, HideReferenceObjectPicker]
        internal Config _value;

        public Config CopyValue()
        {
            return _value.Clone();
        }

#if UNITY_EDITOR
        private bool IsValid { get; set; } = true;
        private string ErrorMessage { get; set; } = "";
        
        [Button("Validate"), HideIf(nameof(IsValid))]
        [InfoBox("$ErrorMessage", InfoMessageType = InfoMessageType.Error)]
        private void OnValidate()
        {
            Validate(_value);
        }

        private void Validate(Config value)
        {
            try
            {
                value.Validate();
                ErrorMessage = "";
                IsValid = true;
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                IsValid = false;
            }
        }
#endif
    }
}