using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Infrastructure.Configuration.Containers
{
    public class ConfigContainer : ScriptableObject
    {
        [SerializeField, ReadOnly, ListDrawerSettings(IsReadOnly = true)]
        internal List<ConfigAsset> _configs;

        public IEnumerable<Config> GetAll()
        {
            return _configs.Select(x => x.CopyValue());
        }
    }
}