using System;
using System.Collections.Generic;

namespace Infrastructure.Analytics
{
    /// <summary>
    /// Listens for signals and relays them to one or more analytics implementations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class AnalyticsController<T>
    {
        public bool Enabled { get; set; } = true;
        
        private readonly HashSet<T> _implementations = new HashSet<T>();

        public abstract void Subscribe(object signalBus);
        
        public void AddImplementation(T implementation)
        {
            _implementations.Add(implementation);
        }

        protected void Process(Action<T> call)
        {
            if (!Enabled) return;
            
            foreach (var listener in _implementations)
            {
                ProcessListener(call, listener);
            }
        }

        private static void ProcessListener(Action<T> call, T implementation)
        {
            try
            {
                call.Invoke(implementation);
            }
            catch (Exception e)
            {
                //TODO: send to log
            }
        }
    }
}