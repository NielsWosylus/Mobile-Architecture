﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Infrastructure.Persistence.Attributes;

namespace Infrastructure.Persistence
{
    internal class TypeMapper
    {
        private readonly Dictionary<SaveInfo, Type> _dataTypes = new();
        private readonly Dictionary<Type, SaveUpgrader> _upgraders = new();

        public TypeMapper(IEnumerable<Assembly> supportedAssemblies)
        {
            foreach (var assembly in supportedAssemblies)
            {
                MapTypesFrom(assembly);
            }
        }

        public bool HasUpgrader(Type returnType)
        {
            return _upgraders.ContainsKey(returnType);
        }
        
        /// <summary>
        /// Returns the type of an upgrader that can return a value of the specified type
        /// </summary>
        public SaveUpgrader GetUpgrader(Type returnType)
        {
            if (!_upgraders.ContainsKey(returnType))
                throw new ArgumentException($"No upgrader returns type of {returnType}");
            
            return _upgraders[returnType];
        }
        
        private void MapTypesFrom(Assembly assembly)
        {
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                if(type.GetCustomAttribute<VersionedSave>() != null)
                    MapSaveType(type);

                if (typeof(SaveUpgrader).IsAssignableFrom(type))
                    MapUpgrader(type);
            }
        }

        private void MapSaveType(Type type)
        {
            var info = SaveInfo.From(type);
            if (_dataTypes.ContainsKey(info))
                throw new ArgumentException($"A type for {info} has already been mapped");
            
            _dataTypes.Add(info, type);
        }
        
        private void MapUpgrader(Type type)
        {
            var upgrader = (SaveUpgrader)Activator.CreateInstance(type);
            var outputType = upgrader.GetOutputType();
            if(_upgraders.ContainsKey(outputType))
                throw new ArgumentException($"An upgrader which returns type of {outputType} has already been mapped");
                
            _upgraders.Add(outputType, upgrader);
        }
    }
}