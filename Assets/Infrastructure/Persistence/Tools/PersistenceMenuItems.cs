﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Infrastructure.Persistence.Tools
{
    internal static class PersistenceMenuItems
    {
        private static string FolderPath = Path.Join(Application.persistentDataPath, "Saves");
        
        [MenuItem("Persistence/Open save folder")]
        public static void OpenFolder()
        {
            if (!Directory.Exists(FolderPath))
                Directory.CreateDirectory(FolderPath);
            
            EditorUtility.RevealInFinder($"{FolderPath}/");
        }
        
        [MenuItem("Persistence/Clear save folder")]
        public static void Clear()
        {
            FileUtil.DeleteFileOrDirectory(FolderPath);
        }
    }
}