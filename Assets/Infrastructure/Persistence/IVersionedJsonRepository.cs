﻿using Cysharp.Threading.Tasks;

namespace Infrastructure.Persistence
{
    public interface IVersionedJsonRepository
    {
        UniTask<VersionedJson> Get(string featureName);
        UniTask Save(VersionedJson versionedJson);
    }
}