﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Cysharp.Threading.Tasks;

namespace Infrastructure.Persistence
{
    public interface ISaveRepository
    {
        UniTask<T> Get<T>();
        UniTask Save<T>(T value);
    }

    public class SaveRepository : ISaveRepository
    {
        private readonly IVersionedJsonRepository _localRepository;
        private readonly IVersionedJsonRepository _cloudRepository;
        private readonly TypeMapper _typeMapper;

        private readonly Dictionary<Type, object> _cache = new();

        public SaveRepository(
            IVersionedJsonRepository localRepository, 
            IVersionedJsonRepository cloudRepository,
            IEnumerable<Assembly> supportedAssemblies)
        {
            _localRepository = localRepository;
            _cloudRepository = cloudRepository;
            _typeMapper = new TypeMapper(supportedAssemblies);
        }
        
        public async UniTask Save<T>(T value)
        {
            var versionedJson = VersionedJson.From(value);
            Cache(value);
            
            await _localRepository.Save(versionedJson);
            await _cloudRepository.Save(versionedJson);
        }

        public async UniTask<T> Get<T>()
        {
            var result = await Get(typeof(T));
            return (T)result;
        }

        private async UniTask<object> Get(Type type)
        {
            if (_cache.ContainsKey(type))
                return _cache[type];
            
            var resultInfo = SaveInfo.From(type);
            var resultFeatureName = resultInfo.FeatureName;
            var versionedJson = await _localRepository.Get(resultFeatureName);

            if (versionedJson.Matches(resultInfo))
            {
                var matchingResult = versionedJson.DeserializeInto(type);
                Cache(matchingResult);
                return matchingResult;
            }

            var upgradedResult = await UpgradeInto(type);
            if (upgradedResult != null)
            {
                await Save(upgradedResult);
                return upgradedResult;    
            }
            
            var newResult = BuildNew(type);
            await Save(newResult);
            return newResult;
        }
        
        private async UniTask<object> UpgradeInto(Type type)
        {
            if (_cache.ContainsKey(type))
                return _cache[type];
            
            if (!_typeMapper.HasUpgrader(type))
                return null;
            
            var upgrader = _typeMapper.GetUpgrader(type);
            var inputTypes = upgrader.GetInputTypes();
            var inputs = new object[inputTypes.Length];
            for (int i = 0; i < inputTypes.Length; i++)
            {
                var inputType = inputTypes[i];
                var resultInfo = SaveInfo.From(inputType);
                var resultFeatureName = resultInfo.FeatureName;
                var versionedJson = await _localRepository.Get(resultFeatureName);
                
                if (versionedJson.Matches(resultInfo))
                {
                    inputs[i] = versionedJson.DeserializeInto(inputType);
                    Cache(inputs[i]);
                }
                else
                {
                    inputs[i] = await UpgradeInto(inputType);
                    if (inputs[i] == null)
                        return null;
                }
            }

            return upgrader.Upgrade(inputs);
        }

        private void Cache(object value)
        {
            var type = value.GetType();
            if (_cache.ContainsKey(type))
                _cache[type] = value;
            else _cache.Add(type, value);
        }
        
        private static object BuildNew(Type type)
        {
            return Activator.CreateInstance(type);
        }
    }
}