﻿using Infrastructure.Persistence.Attributes;

namespace Infrastructure.Persistence.Tests.Saves
{
    [VersionedSave("A", 1)]
    public class A1
    {
        public string Value = "A";
    }

    [VersionedSave("B", 1)]
    public class B1
    {
        public string Value = "B";
    }

    [VersionedSave("C", 1)]
    public class C1
    {
        public string ValueA;
        public string ValueB;
    }
    
    [VersionedSave("C", 2)]
    public class C2
    {
        public string ValueA;
        public string ValueB;
    }
    
    [VersionedSave("IntoC3", 1)]
    public class IntoC3
    {
        public string Value = "IntoC3";
    }
    
    [VersionedSave("C", 3)]
    public class C3
    {
        public string ValueA;
        public string ValueB;
        public string ValueIntoC;
    }
    
    [VersionedSave("CC", 1)]
    public class CC1
    {
        public string Value = "CC";
    }
    
    [VersionedSave("CC", 2)]
    public class CC2
    {
        public string Value;
    }
    
    [VersionedSave("D", 1)]
    public class D1
    {
        public string ValueA;
        public string ValueB;
        public string ValueIntoC;
        public string ValueCC;
    }
    
    [VersionedSave("IntoD2", 1)]
    public class IntoD2
    {
        public string Value = "IntoD2";
    }
    
    [VersionedSave("D", 2)]
    public class D2
    {
        public string ValueA;
        public string ValueB;
        public string ValueIntoC;
        public string ValueCC;
        public string ValueIntoD;
    }
    
    [VersionedSave("D", 3)]
    public class D3
    {
        public string ValueA;
        public string ValueB;
        public string ValueIntoC;
        public string ValueCC;
        public string ValueIntoD;
    }
    
    [VersionedSave("E", 1)]
    public class E1
    {
        public string ValueA;
        public string ValueB;
        public string ValueIntoC;
        public string ValueCC;
        public string ValueIntoD;
    }
    
    [VersionedSave("E", 2)]
    public class E2
    {
        public string ValueA;
        public string ValueB;
        public string ValueIntoC;
        public string ValueCC;
        public string ValueIntoD;
    }
    
    [VersionedSave("F", 1)]
    public class F1
    {
        public string Value;
    }
    
    [VersionedSave("E", 3)]
    public class E3
    {
        public string ValueA;
        public string ValueB;
        public string ValueIntoC;
        public string ValueCC;
        public string ValueIntoD;
        public string ValueF;
    }
    
    public class C1Upgrader : SaveUpgrader<A1, B1, C1>
    {
        protected override C1 Upgrade(A1 dataA, B1 dataB)
        {
            return new C1
            {
                ValueA = dataA.Value,
                ValueB = dataB.Value
            };
        }
    }
    
    public class C2Upgrader : SaveUpgrader<C1, C2>
    {
        protected override C2 Upgrade(C1 data)
        {
            return new C2
            {
                ValueA = data.ValueA,
                ValueB = data.ValueB
            };
        }
    }
    
    public class C3Upgrader : SaveUpgrader<C2, IntoC3, C3>
    {
        protected override C3 Upgrade(C2 dataA, IntoC3 dataB)
        {
            return new C3
            {
                ValueA = dataA.ValueA,
                ValueB = dataA.ValueB,
                ValueIntoC = dataB.Value
            };
        }
    }

    public class CC2Upgrader : SaveUpgrader<CC1, CC2>
    {
        protected override CC2 Upgrade(CC1 data)
        {
            return new CC2
            {
                Value = data.Value
            };
        }
    }

    public class D1Upgrader : SaveUpgrader<C3, CC2, D1>
    {
        protected override D1 Upgrade(C3 dataA, CC2 dataB)
        {
            return new D1
            {
                ValueA = dataA.ValueA,
                ValueB = dataA.ValueB,
                ValueIntoC = dataA.ValueIntoC,
                ValueCC = dataB.Value
            };
        }
    }
    
    public class D2Upgrader : SaveUpgrader<D1, IntoD2, D2>
    {
        protected override D2 Upgrade(D1 dataA, IntoD2 dataB)
        {
            return new D2
            {
                ValueA = dataA.ValueA,
                ValueB = dataA.ValueB,
                ValueIntoC = dataA.ValueIntoC,
                ValueCC = dataA.ValueCC,
                ValueIntoD = dataB.Value,
            };
        }
    }

    public class D3Upgrader : SaveUpgrader<D2, D3>
    {
        protected override D3 Upgrade(D2 data)
        {
            return new D3
            {
                ValueA = data.ValueA,
                ValueB = data.ValueB,
                ValueIntoC = data.ValueIntoC,
                ValueCC = data.ValueCC,
                ValueIntoD = data.ValueIntoD
            };
        }
    }
    
    public class E1Upgrader : SaveUpgrader<D3, E1>
    {
        protected override E1 Upgrade(D3 data)
        {
            return new E1
            {
                ValueA = data.ValueA,
                ValueB = data.ValueB,
                ValueIntoC = data.ValueIntoC,
                ValueCC = data.ValueCC,
                ValueIntoD = data.ValueIntoD
            };
        }
    }
    
    public class E2Upgrader : SaveUpgrader<E1, E2>
    {
        protected override E2 Upgrade(E1 data)
        {
            return new E2
            {
                ValueA = data.ValueA,
                ValueB = data.ValueB,
                ValueIntoC = data.ValueIntoC,
                ValueCC = data.ValueCC,
                ValueIntoD = data.ValueIntoD
            };
        }
    }
    
    public class F1Upgrader : SaveUpgrader<E1, F1>
    {
        protected override F1 Upgrade(E1 data)
        {
            return new F1
            {
                Value = data.ValueA + data.ValueB + data.ValueCC + data.ValueIntoC + data.ValueIntoD
            };
        }
    }

    public class E3Upgrader : SaveUpgrader<E2, F1, E3>
    {
        protected override E3 Upgrade(E2 dataA, F1 dataB)
        {
            return new E3
            {
                ValueA = dataA.ValueA,
                ValueB = dataA.ValueB,
                ValueIntoC = dataA.ValueIntoC,
                ValueCC = dataA.ValueCC,
                ValueIntoD = dataA.ValueIntoD,
                ValueF = dataB.Value,
            };
        }
    }

    [VersionedSave("P", 1)]
    public class P1
    {
        public string ValueB;
    }
    
    [VersionedSave("P", 2)]
    public class P2
    {
        public string ValueB;
        public string ValueCC;
    }
    
    [VersionedSave("P", 3)]
    public class P3
    {
        public string ValueB;
        public string ValueCC;
        public string ValueD;
    }
    
    [VersionedSave("P", 4)]
    public class P4
    {
        public string ValueB;
        public string ValueCC;
        public string ValueD;
        public string ValueF;
    }
    
    [VersionedSave("P", 5)]
    public class P5
    {
        public string ValueB;
        public string ValueCC;
        public string ValueD;
        public string ValueF;
        public string ValueE;
    }

    public class P1Upgrader : SaveUpgrader<B1, P1>
    {
        protected override P1 Upgrade(B1 data)
        {
            return new P1
            {
                ValueB = data.Value
            };
        }
    }

    public class P2Upgrader : SaveUpgrader<P1, CC1, P2>
    {
        protected override P2 Upgrade(P1 dataA, CC1 dataB)
        {
            return new P2
            {
                ValueB = dataA.ValueB,
                ValueCC = dataB.Value
            };
        }
    }

    public class P3Upgrader : SaveUpgrader<P2, D2, P3>
    {
        protected override P3 Upgrade(P2 dataA, D2 dataB)
        {
            return new P3
            {
                ValueB = dataA.ValueB,
                ValueCC = dataA.ValueCC,
                ValueD = dataB.ValueA + dataB.ValueB + dataB.ValueCC + dataB.ValueIntoC + dataB.ValueIntoD
            };
        }
    }

    public class P4Upgrader : SaveUpgrader<P3, F1, P4>
    {
        protected override P4 Upgrade(P3 dataA, F1 dataB)
        {
            return new P4
            {
                ValueB = dataA.ValueB,
                ValueCC = dataA.ValueCC,
                ValueD = dataA.ValueD,
                ValueF = dataB.Value
            };
        }
    }

    public class P5Upgrader : SaveUpgrader<P4, E3, P5>
    {
        protected override P5 Upgrade(P4 dataA, E3 dataB)
        {
            return new P5
            {
                ValueB = dataA.ValueB,
                ValueCC = dataA.ValueCC,
                ValueD = dataA.ValueD,
                ValueF = dataA.ValueF,
                ValueE = dataB.ValueA + dataB.ValueB + dataB.ValueCC + dataB.ValueIntoC +
                         dataB.ValueIntoD + dataB.ValueF
            };
        }
    }
}