﻿using Infrastructure.Persistence;
using Infrastructure.Persistence.Attributes;

namespace Persistence.UnitTests
{
    [VersionedSave("BranchA", 1)]
    public class MergingSaveV1A
    {
        public string StringFromA;
    }
    
    [VersionedSave("BranchB", 1)]
    public class MergingSaveV1B
    {
        public string StringFromB;
    }
    
    [VersionedSave("Merged", 1)]
    public class MergedSaveV1
    {
        public string CombinedString;
    }
    
    public class MergedSaveUpgraderV1 : SaveUpgrader<MergingSaveV1A, MergingSaveV1B, MergedSaveV1>
    {
        protected override MergedSaveV1 Upgrade(MergingSaveV1A dataA, MergingSaveV1B dataB)
        {
            return new MergedSaveV1
            {
                CombinedString = dataA.StringFromA + dataB.StringFromB
            };
        }
    }
}