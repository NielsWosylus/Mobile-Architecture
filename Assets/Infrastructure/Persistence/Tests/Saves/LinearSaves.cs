﻿using System;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Attributes;

namespace Persistence.UnitTests
{
    [VersionedSave("PreLinear", 1)]
    public class PreLinearSaveV1
    {
        public string Value;
    }
    
    [VersionedSave("Linear", 1)]
    public class LinearSaveV1
    {
        public string V1String;
    }
    
    [VersionedSave("Linear", 2)]
    public class LinearSaveV2
    {
        public string V2String;
    }
    
    [VersionedSave("Linear", 3)]
    public class LinearSaveV3
    {
        public string V3String;
        public int V3Int = 5;
    }

    public class LinearSaveUpgraderV1 : SaveUpgrader<PreLinearSaveV1, LinearSaveV1>
    {
        protected override LinearSaveV1 Upgrade(PreLinearSaveV1 data)
        {
            return new LinearSaveV1
            {
                V1String = data.Value
            };
        }
    }
    
    public class LinearSaveUpgraderV2 : SaveUpgrader<LinearSaveV1, LinearSaveV2>
    {
        protected override LinearSaveV2 Upgrade(LinearSaveV1 data)
        {
            return new LinearSaveV2
            {
                V2String = data.V1String
            };
        }
    }
    
    public class LinearSaveUpgraderV3 : SaveUpgrader<LinearSaveV2, LinearSaveV3>
    {
        protected override LinearSaveV3 Upgrade(LinearSaveV2 data)
        {
            return new LinearSaveV3
            {
                V3String = data.V2String,
                V3Int = data.V2String.Length
            };
        }
    }
}