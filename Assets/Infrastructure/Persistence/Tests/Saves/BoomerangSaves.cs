﻿using Infrastructure.Persistence.Attributes;

namespace Infrastructure.Persistence.Tests.Saves
{
    [VersionedSave("Main", 1)]
    public class BoomerangMain1
    {
        public string Value;
    }

    [VersionedSave("Main", 2)]
    public class BoomerangMain2
    {
        public string Value;
    }

    [VersionedSave("Off", 1)]
    public class BoomerangOff1
    {
        public string Value;
    }

    public class BoomerangOff1Upgrader : SaveUpgrader<BoomerangMain1, BoomerangOff1>
    {
        protected override BoomerangOff1 Upgrade(BoomerangMain1 data)
        {
            return new BoomerangOff1
            {
                Value = data.Value
            };
        }
    }

    public class BoomerangMain2Upgrader : SaveUpgrader<BoomerangMain1, BoomerangOff1, BoomerangMain2>
    {
        protected override BoomerangMain2 Upgrade(BoomerangMain1 dataA, BoomerangOff1 dataB)
        {
            return new BoomerangMain2
            {
                Value = dataA.Value + dataB.Value
            };
        }
    }
}