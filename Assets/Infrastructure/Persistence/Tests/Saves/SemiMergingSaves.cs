﻿using System;
using Infrastructure.Persistence;
using Infrastructure.Persistence.Attributes;

namespace Persistence.UnitTests
{
    [VersionedSave("MainBranch", 1)]
    public class MainBranchV1
    {
        public string StringFromA;
    }
    
    [VersionedSave("AddedBranch", 1)]
    public class AddedBranchV1
    {
        public string StringFromB;
    }
    
    [VersionedSave("MainBranch", 2)]
    public class MainBranchV2
    {
        public string MainString = "default";
        public string AddedString = "default";
    }
    
    public class SemiBranchingSaveUpgraderV2 : SaveUpgrader<MainBranchV1, AddedBranchV1, MainBranchV2>
    {
        protected override MainBranchV2 Upgrade(MainBranchV1 dataA, AddedBranchV1 dataB)
        {
            return new MainBranchV2
            {
                MainString = dataA.StringFromA,
                AddedString = dataB.StringFromB
            };
        }
    }
}