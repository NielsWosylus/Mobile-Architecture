﻿using Infrastructure.Persistence.Attributes;

namespace Infrastructure.Persistence.Tests.Saves
{
    [VersionedSave("BranchingMain", 1)]
    public class BranchingSaveV1
    {
        public string Value;
    }

    [VersionedSave("BranchingMain", 2)]
    public class BranchingSaveV2
    {
        public string Value;
    }
    
    [VersionedSave("BranchedA", 1)]
    public class BranchedSaveA1
    {
        public string Value;
    }

    [VersionedSave("BranchedB", 1)]
    public class BranchedSaveB1
    {
        public string Value;
    }

    public class BranchA1Upgrader : SaveUpgrader<BranchingSaveV1, BranchedSaveA1>
    {
        protected override BranchedSaveA1 Upgrade(BranchingSaveV1 data)
        {
            return new BranchedSaveA1
            {
                Value = data.Value
            };
        }
    }
    
    public class BranchB1Upgrader : SaveUpgrader<BranchingSaveV1, BranchedSaveB1>
    {
        protected override BranchedSaveB1 Upgrade(BranchingSaveV1 data)
        {
            return new BranchedSaveB1
            {
                Value = data.Value
            };
        }
    }
    
    public class BranchingSaveV2Upgrader : SaveUpgrader<BranchingSaveV1, BranchingSaveV2>
    {
        protected override BranchingSaveV2 Upgrade(BranchingSaveV1 data)
        {
            return new BranchingSaveV2
            {
                Value = data.Value
            };
        }
    }
}