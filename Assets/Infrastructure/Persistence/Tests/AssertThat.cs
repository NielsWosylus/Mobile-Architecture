﻿using System;
using NUnit.Framework;

namespace Infrastructure.Persistence.Tests
{
    internal static class AssertThat
    {
        public static void MatchesNew<T>(T obj) where T : new()
        {
            Assert.That(obj, Is.Not.Null);
            
            var newObj = new T();
            var fields = typeof(T).GetFields();
            
            foreach (var field in fields)
            {
                var valueObject = field.GetValue(obj);
                var valueNew = field.GetValue(newObj);
                
                if (valueObject == null && valueNew == null)
                    continue;
                
                Assert.That(valueObject, Is.Not.Null);
                Assert.That(newObj, Is.Not.Null);

                Assert.That(valueObject, Is.EqualTo(valueNew));
            }
        }
        
        public static void ThereIsMatchBetween<T>(T a, T b)
        {
            Assert.That(a, Is.Not.Null);
            Assert.That(b, Is.Not.Null);
            
            var fields = typeof(T).GetFields();
            foreach (var field in fields)
            {
                var valueA = field.GetValue(a);
                var valueB = field.GetValue(b);
                
                if (valueA == null && valueB == null)
                    continue;
                
                Assert.That(valueA, Is.Not.Null);
                Assert.That(valueA, Is.Not.Null);

                Assert.That(valueA, Is.EqualTo(valueB));
            }
        }
    }
}