﻿using Infrastructure.Persistence.Attributes;
using Infrastructure.Persistence.Exceptions;
using NUnit.Framework;

namespace Infrastructure.Persistence.Tests
{
    [TestFixture]
    public class SaveInfoTests
    {
        [Test]
        public void From_TypeHasNamedAttribute_ReturnsFeatureName()
        {
            var resultA = SaveInfo.From(typeof(NamedSave));
            var resultB = SaveInfo.From(typeof(NamedSaveV13));
            
            Assert.That(resultA.FeatureName, Is.EqualTo("Named"));
            Assert.That(resultB.FeatureName, Is.EqualTo("Named"));
        }
        
        [Test]
        public void From_TypeHasUnnamedAttribute_ReturnsFeatureName()
        {
            var resultA = SaveInfo.From(typeof(UnnamedSave));
            var resultB = SaveInfo.From(typeof(UnnamedSaveV2));
            
            Assert.That(resultA.FeatureName, Is.EqualTo("Unnamed"));
            Assert.That(resultB.FeatureName, Is.EqualTo("Unnamed"));
        }

        [Test]
        public void From_TypeIsMissingAttribute_ThrowsUnsupportedType()
        {
            Assert.Throws<UnsupportedTypeException>(() => SaveInfo.From(typeof(UnmarkedSave)));
        }
    }

    [VersionedSave(1)]
    public class UnnamedSave { }
    
    [VersionedSave(2)]
    public class UnnamedSaveV2 { }
    
    [VersionedSave("Named", 1)]
    public class NamedSave { }
    
    [VersionedSave("Named", 13)]
    public class NamedSaveV13 { }
    
    public class UnmarkedSave { }
}