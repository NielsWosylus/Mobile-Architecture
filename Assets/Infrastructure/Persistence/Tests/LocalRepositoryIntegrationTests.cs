﻿using Cysharp.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using Persistence;
using UnityEditor;

namespace Infrastructure.Persistence.Tests
{
    [TestFixture]
    public class LocalRepositoryIntegrationTests : SaveRepositoryTests
    {
        [SetUp]
        public override void SetUp()
        {
            _localRepository = new LocalJsonRepository("Tests");
            _cloudRepository = Substitute.For<IVersionedJsonRepository>();
            var assembly = typeof(SaveRepositoryTests).Assembly;

            _repository = new SaveRepository(_localRepository, _cloudRepository, new[] { assembly });
        }

        protected override async UniTask StoreInLocalRepo(params object[] saves)
        {
            foreach (var save in saves)
            {
                var versionedJson = VersionedJson.From(save);
                await _localRepository.Save(versionedJson);
            }
        }

        [TearDown]
        public void TearDown()
        {
            var localRepo = (LocalJsonRepository)_localRepository;
            var path = localRepo.DirectoryPath;
            FileUtil.DeleteFileOrDirectory(path);
        }
    }
}