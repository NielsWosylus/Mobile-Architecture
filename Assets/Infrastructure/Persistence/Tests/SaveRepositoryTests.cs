﻿using System.Collections;
using Cysharp.Threading.Tasks;
using Infrastructure.Persistence.Tests.Saves;
using NSubstitute;
using NUnit.Framework;
using Persistence;
using Persistence.UnitTests;
using UnityEngine.TestTools;

namespace Infrastructure.Persistence.Tests
{
    [TestFixture]
    public class SaveRepositoryTests
    {
        protected ISaveRepository _repository;
        protected IVersionedJsonRepository _localRepository;
        protected IVersionedJsonRepository _cloudRepository;

        [SetUp]
        public virtual void SetUp()
        {
            _localRepository = Substitute.For<IVersionedJsonRepository>();
            _cloudRepository = Substitute.For<IVersionedJsonRepository>();
            var assembly = typeof(SaveRepositoryTests).Assembly;

            _repository = new SaveRepository(_localRepository, _cloudRepository, new[] { assembly });
        }

        [UnityTest]
        public IEnumerator Get_LinearHasMatch() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(new LinearSaveV3 { V3String = "string value", V3Int = 15 });
            
            var result = await _repository.Get<LinearSaveV3>();
            
            Assert.That(result.V3String, Is.EqualTo("string value"));
            Assert.That(result.V3Int, Is.EqualTo(15));
        });
        
        [UnityTest]
        public IEnumerator Get_LinearHasPreviousVersion() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(new LinearSaveV1 { V1String = "string value" });
            
            var result = await _repository.Get<LinearSaveV3>();
            
            Assert.That(result.V3String, Is.EqualTo("string value"));
        });

        [UnityTest]
        public IEnumerator Get_LinearHasPreviousFeature() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(new PreLinearSaveV1 { Value = "string value" });

            var result = await _repository.Get<LinearSaveV3>();

            Assert.That(result.V3String, Is.EqualTo("string value"));
        });

        [UnityTest]
        public IEnumerator Get_SemiMergedHasAll() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(
                new MainBranchV1 { StringFromA = "A" },
                new AddedBranchV1 { StringFromB = "B" });

            var result = await _repository.Get<MainBranchV2>();

            Assert.That(result.MainString, Is.EqualTo("A"));
            Assert.That(result.AddedString, Is.EqualTo("B"));
        });

        [UnityTest]
        public IEnumerator Get_MergedHasAll() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(
                new MergingSaveV1A { StringFromA = "A" },
                new MergingSaveV1B { StringFromB = "B" });

            var result = await _repository.Get<MergedSaveV1>();

            Assert.That(result.CombinedString, Is.EqualTo("AB"));
        });

        [UnityTest]
        public IEnumerator Get_BoomerangHasAll() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(new BoomerangMain1 { Value = "A" });

            var result = await _repository.Get<BoomerangMain2>();

            Assert.That(result.Value, Is.EqualTo("AA"));
        });

        [UnityTest]
        public IEnumerator Get_BranchingHasAll() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(new BranchingSaveV1 { Value = "A" });

            var resultMain = await _repository.Get<BranchingSaveV2>();
            var resultA = await _repository.Get<BranchedSaveA1>();
            var resultB = await _repository.Get<BranchedSaveB1>();

            Assert.That(resultMain.Value, Is.EqualTo("A"));
            Assert.That(resultA.Value, Is.EqualTo("A"));
            Assert.That(resultB.Value, Is.EqualTo("A"));
        });

        [UnityTest]
        public IEnumerator Get_ComplexHasAll() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(
                new A1 { Value = "A" },
                new B1 { Value = "B" },
                new CC1 { Value = "CC" },
                new IntoC3 { Value = "IntoC3" },
                new IntoD2 { Value = "IntoD2" });

            var resultE3 = await _repository.Get<E3>();
            var resultP5 = await _repository.Get<P5>();

            var expectedE3 = new E3
            {
                ValueA = "A", ValueB = "B", ValueIntoC = "IntoC3", ValueCC = "CC", ValueIntoD = "IntoD2",
                ValueF = "ABCCIntoC3IntoD2"
            };

            var expectedP5 = new P5
            {
                ValueB = "B", ValueCC = "CC", ValueD = "ABCCIntoC3IntoD2", ValueF = "ABCCIntoC3IntoD2",
                ValueE = "ABCCIntoC3IntoD2ABCCIntoC3IntoD2"
            };

            AssertThat.ThereIsMatchBetween(resultE3, expectedE3);
            AssertThat.ThereIsMatchBetween(resultP5, expectedP5);
        });
        
        [UnityTest]
        public IEnumerator Get_LinearHasNone_ReturnsNew() => UniTask.ToCoroutine(async () =>
        {
            var result = await _repository.Get<LinearSaveV3>();
            
            AssertThat.MatchesNew(result);
        });
        
        [UnityTest]
        public IEnumerator Get_SemiMergedHasOne_ReturnsNew() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(new AddedBranchV1 { StringFromB = "B" });

            var result = await _repository.Get<MainBranchV2>();

            AssertThat.MatchesNew(result);
        });
        
        [UnityTest]
        public IEnumerator Get_SemiMergedHasNone_ReturnsNew() => UniTask.ToCoroutine(async () =>
        {
            var result = await _repository.Get<MainBranchV2>();

            AssertThat.MatchesNew(result);
        });
        
        [UnityTest]
        public IEnumerator Get_MergedHasOne_ReturnsNew() => UniTask.ToCoroutine(async () =>
        {
            await StoreInLocalRepo(new MergingSaveV1A { StringFromA = "A" });

            var result = await _repository.Get<MergedSaveV1>();

            AssertThat.MatchesNew(result);
        });
        
        [UnityTest]
        public IEnumerator Get_MergedHasNone_ReturnsNew() => UniTask.ToCoroutine(async () =>
        {
            var result = await _repository.Get<MergedSaveV1>();

            AssertThat.MatchesNew(result);
        });
        
        [UnityTest]
        public IEnumerator Get_BoomerangHasNone_ReturnsNew() => UniTask.ToCoroutine(async () =>
        {
            var result = await _repository.Get<BoomerangMain2>();

            AssertThat.MatchesNew(result);
        });
        
        [UnityTest]
        public IEnumerator Get_BranchingHasNone_ReturnsNew() => UniTask.ToCoroutine(async () =>
        {
            var resultMain = await _repository.Get<BranchingSaveV2>();
            var resultA = await _repository.Get<BranchedSaveA1>();
            var resultB = await _repository.Get<BranchedSaveB1>();
            
            AssertThat.MatchesNew(resultMain);
            AssertThat.MatchesNew(resultA);
            AssertThat.MatchesNew(resultB);
        });
        
        protected virtual UniTask StoreInLocalRepo(params object[] saves)
        {
            foreach (var save in saves)
            {
                var versionedJson = VersionedJson.From(save);
                var task = new UniTask<VersionedJson>(versionedJson);
                _localRepository.Get(versionedJson.FeatureName).Returns(task);
            }
            
            return UniTask.CompletedTask;
        }
    }
}