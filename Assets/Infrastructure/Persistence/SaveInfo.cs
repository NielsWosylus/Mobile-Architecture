﻿using System;
using Infrastructure.Persistence.Attributes;
using Infrastructure.Persistence.Exceptions;
using Sirenix.Utilities;

namespace Infrastructure.Persistence
{
    public readonly struct SaveInfo
    {
        public readonly string FeatureName;
        public readonly int Version;

        private SaveInfo(string featureName, int version)
        {
            FeatureName = featureName;
            Version = version;
        }

        public static SaveInfo From(Type type)
        {
            var attribute = type.GetCustomAttribute<VersionedSave>();
            if(attribute == null)
                throw new UnsupportedTypeException($"{type} cannot represent a versioned save, as it is not marked with a VersionedSave attribute");
                
            if(!string.IsNullOrEmpty(attribute.FeatureName))
                return new SaveInfo(attribute.FeatureName, attribute.Version);
            
            var typeName = type.Name;
            if (!typeName.Contains("Save"))
                throw new UnsupportedTypeException($"{type} cannot represent a versioned save. Please supply a feature name in the VersionedSave attribute, or name the class using the following format: [FeatureName]Save[OptionalSuffix]");
            
            var suffixIndex = typeName.IndexOf("Save", StringComparison.Ordinal);
            var featureName = typeName.Remove(suffixIndex);
            return new SaveInfo(featureName, attribute.Version);
        }

        public override bool Equals(object obj)
        {
            return obj is SaveInfo other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(FeatureName, Version);
        }

        public override string ToString()
        {
            return $"{FeatureName}-v{Version}";
        }
        
        private bool Equals(SaveInfo other)
        {
            return FeatureName == other.FeatureName && Version == other.Version;
        }
    }
}