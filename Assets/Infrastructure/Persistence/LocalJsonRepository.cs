﻿using System.Collections.Generic;
using System.IO;
using Cysharp.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;

namespace Infrastructure.Persistence
{
    internal class LocalJsonRepository : IVersionedJsonRepository
    {
        internal string DirectoryPath { get; }

        private readonly Dictionary<string, VersionedJson> _cache = new();

        public LocalJsonRepository(string folderName)
        {
            DirectoryPath = Path.Join(Application.persistentDataPath, $"Saves/{folderName}");
        }

        public async UniTask<VersionedJson> Get(string featureName)
        {
            if (_cache.ContainsKey(featureName))
                return _cache[featureName];
                
            var filePath = GetFilePath(featureName);
            if (!File.Exists(filePath))
            {
                _cache.Add(featureName, VersionedJson.Empty);
                return VersionedJson.Empty;   
            }

            var json = await File.ReadAllTextAsync(filePath);
            var versionedJson = JsonConvert.DeserializeObject<VersionedJson>(json);
            Cache(versionedJson);
            return versionedJson;
        }

        public async UniTask Save(VersionedJson versionedJson)
        {
            var path = GetFilePath(versionedJson.FeatureName);
            var json = JsonConvert.SerializeObject(versionedJson);
            await File.WriteAllTextAsync(path, json);
            Cache(versionedJson);
        }

        private void Cache(VersionedJson json)
        {
            if (_cache.ContainsKey(json.FeatureName))
                _cache[json.FeatureName] = json;
            else _cache.Add(json.FeatureName, json);
        }

        private string GetFilePath(string featureName)
        {
            if(!Directory.Exists(DirectoryPath))
                Directory.CreateDirectory(DirectoryPath);

            return Path.Join(DirectoryPath, $"{featureName}.json");
        }
    }
}