using System;

namespace Infrastructure.Persistence.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class VersionedSave : Attribute
    {
        public readonly int Version;
        public readonly string FeatureName;

        public VersionedSave(int version)
        {
            Version = version;
        }
        
        public VersionedSave(string featureName, int version)
        {
            Version = version;
            FeatureName = featureName;
        }
    }
}