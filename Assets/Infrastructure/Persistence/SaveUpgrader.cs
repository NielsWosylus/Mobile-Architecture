using System;

namespace Infrastructure.Persistence
{
    public abstract class SaveUpgrader
    {
        public abstract Type[] GetInputTypes();
        public abstract Type GetOutputType();
        public abstract object Upgrade(object[] inputs);
    }
    
    public abstract class SaveUpgrader<TOld, TNew> : SaveUpgrader
    {
        protected abstract TNew Upgrade(TOld data);

        public override Type[] GetInputTypes()
        {
            return new[] { typeof(TOld) };
        }

        public override Type GetOutputType()
        {
            return typeof(TNew);
        }

        public override object Upgrade(object[] inputs)
        {
            var input = (TOld)inputs[0];
            return Upgrade(input);
        }
    }
    
    public abstract class SaveUpgrader<TOld1, TOld2, TNew> : SaveUpgrader
    {
        protected abstract TNew Upgrade(TOld1 dataA, TOld2 dataB);

        public override Type[] GetInputTypes()
        {
            return new[] { typeof(TOld1), typeof(TOld2) };
        }

        public override Type GetOutputType()
        {
            return typeof(TNew);
        }

        public override object Upgrade(object[] inputs)
        {
            var input1 = (TOld1)inputs[0];
            var input2 = (TOld2)inputs[1];
            return Upgrade(input1, input2);
        }
    }
}