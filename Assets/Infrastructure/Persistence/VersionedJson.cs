﻿using System;
using System.Reflection;
using Infrastructure.Persistence.Attributes;
using Newtonsoft.Json;

namespace Infrastructure.Persistence
{
    public struct VersionedJson
    {
        public string Value;
        public string FeatureName;
        public int SaveVersion;

        public bool Matches(SaveInfo info) =>
            info.FeatureName == FeatureName && info.Version == SaveVersion;

        public object DeserializeInto(Type type)
        {
            return JsonConvert.DeserializeObject(Value, type);
        }
        
        public static VersionedJson Empty => new();

        public static VersionedJson From(object value)
        {
            var type = value.GetType();
            var attribute = type.GetCustomAttribute<VersionedSave>();

            return new VersionedJson
            {
                Value = JsonConvert.SerializeObject(value),
                FeatureName = attribute.FeatureName,
                SaveVersion = attribute.Version,
            };
        }
    }
}