using Infrastructure.Persistence;

namespace Models.Lives.LegacySaves
{
    public class LivesSaveV2
    {
        public string SomeNumberString;
    }

    public class LivesSaveUpgraderV2ToV3 : SaveUpgrader<LivesSaveV2, LivesSave>
    {
        protected override LivesSave Upgrade(LivesSaveV2 oldSave)
        {
            return new LivesSave
            {
                SomeNumber = float.Parse(oldSave.SomeNumberString)
            };
        }
    }
}