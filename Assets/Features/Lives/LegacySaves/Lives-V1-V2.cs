using Infrastructure.Persistence;

namespace Models.Lives.LegacySaves
 {
     public class LivesSaveV1
     {
         public int SomeNumber;
     }
 
     public class LivesSaveUpgraderV1ToV2 : SaveUpgrader<LivesSaveV1, LivesSaveV2>
     {
         protected override LivesSaveV2 Upgrade(LivesSaveV1 oldSave)
         {
             return new LivesSaveV2
             {
                 SomeNumberString = oldSave.SomeNumber.ToString()
             };
         }
     }
 }