using System;
using Infrastructure.Configuration;
using Infrastructure.Configuration.Attributes;
using UnityEngine;

namespace Features.Attempts
{
    [Serializable, Configuration]
    public class AttemptsConfig : Config
    {
        public ScriptableObject SomeObject;

        public string LocalString;

        [RemoteValue]
        public int BaseAmount;
        
        [RemoteValue]
        public int[] SoftCurrencyPerBonusAttempt;

        public override void Validate()
        {
            if(BaseAmount <= 0)
                throw new Exception("Base amount must be higher than 0");
        }
    }
}