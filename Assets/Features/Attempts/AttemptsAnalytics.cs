using Infrastructure.Analytics;

namespace Models.Attempts
{
    public interface IAttemptsAnalytics
    {
        void AttemptStarted(AttemptStartedSignal info);
        void AttemptEnded(AttemptEndedSignal info);
    }
    
    public class AttemptsAnalytics : AnalyticsController<IAttemptsAnalytics>, IAttemptsAnalytics
    {
        public override void Subscribe(object signalBus)
        {
            //signalBus.Subscribe<AttemptStartedSignal>(AttemptStarted);
            //signalBus.Subscribe<AttemptEndedSignal>(AttemptEnded);
        }

        public void AttemptStarted(AttemptStartedSignal info)
        {
            Process(x => x.AttemptStarted(info));
        }

        public void AttemptEnded(AttemptEndedSignal info)
        {
            Process(x => x.AttemptEnded(info));
        }
    }
}