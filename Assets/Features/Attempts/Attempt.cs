namespace Models.Attempts
{
    public class Attempt
    {
        public int CoinsCollected;
        public AttemptStatus Status;
    }

    public enum AttemptStatus
    {
        Lost,
        Won,
        Skipped,
        InProgress,
    }
}