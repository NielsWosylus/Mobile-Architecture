using Features.Attempts;
using Infrastructure.Services;

namespace Models.Attempts
{
    public interface IAttemptsService
    {
        
    }
    
    public class AttemptsService : Service<AttemptsConfig, AttemptsSave>, IAttemptsService
    {
        
    }
}
