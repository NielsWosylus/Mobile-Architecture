using Infrastructure.Messaging;

namespace Models.Attempts
{
    public interface IAttemptsSubscriber : ISubscriber<
        AttemptStartedSignal, 
        AttemptEndedSignal> { }
    
    public class AttemptStartedSignal
    {
        
    }

    public class AttemptEndedSignal
    {
        
    }
}