using Features.NewConfigsForMe;
using Infrastructure.Services;

namespace Features.NewConfigsForMe
{
    public interface INewConfigsForMeService
    {
        
    }
    
    public class NewConfigsForMeService : ConfigurableService<NewConfigsForMeConfig>, INewConfigsForMeService
    {
        
    }
}