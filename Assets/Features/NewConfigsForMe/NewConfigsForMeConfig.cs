using System;
using Infrastructure.Configuration;
using Infrastructure.Configuration.Attributes;

namespace Features.NewConfigsForMe
{
    [Serializable, Configuration]
    public class NewConfigsForMeConfig : Config
    {
        [RemoteValue]
        public string SomeString;

        [RemoteValue]
        public int SomeInt;

        [RemoteValue]
        public string AnotherProp;

        public override void Validate()
        {

        }
    }
}