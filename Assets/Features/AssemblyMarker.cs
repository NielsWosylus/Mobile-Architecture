using Infrastructure.Injection;

[assembly: ScanForFeatures]

namespace Models
{
    public interface IAssemblyMarker { }
}